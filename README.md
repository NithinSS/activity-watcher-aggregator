# Activity Watch Aggregator

**** NOTE: THIS PROJECT IS "VERY" EXPERIMENTAL ****

This is a learning experiment and attempt to create an aggregator for Activity Watch (https://activitywatch.net/) running on multiple client devices. Activity Watch is an open source screen-time and usage tracker that runs as a service and provides an API and web UI for viewing usage statistics. But Activity Watch only runs locally and doesn't send data outside also with concerns to Privacy. This project is an attempt to implement a Activity Watch as a WebService for connecting multiple devices to a user and show an aggregated usage statistics.

 The aggregator have three sub-projects :

1. activity-publisher - Client Side Springboot app which interfaces with Activity Watch locally and a remote Apache Kafka Broker.
2. aggregator-webapp - Client Side Angular App which will provide a frontend user interface to control the behavior of the application and see metrics similar to Activity Watch but also aggregated  for multiple connected devices.
3. aggregator-server -  Server side Springboot app which consumes the Activity Watch events from published to Kafka from the activity-publisher on the client. The server also produces time-limit notifications and provide an API for the client side webapp to read the aggregated activities.



## Running

1. Install Kafka broker on the server and start the aggregator-server on it.
2. Install Activity launcher on the client side and start the activity-publisher.
3. Start the aggregator-webapp.