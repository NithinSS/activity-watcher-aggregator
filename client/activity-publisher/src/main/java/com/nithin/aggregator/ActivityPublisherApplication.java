package com.nithin.aggregator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ActivityPublisherApplication {

	public static void main(String[] args) {
		SpringApplication.run(ActivityPublisherApplication.class, args);
	}

}
