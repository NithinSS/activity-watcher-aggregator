package com.nithin.aggregator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AggregatorServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(AggregatorServerApplication.class, args);
	}

}
